package com.feng.javalearn.stream.api.action;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Main {

    public static void main(String[] args) {
        Trader feng = new Trader("feng","beijing");
        Trader zhang = new Trader("zhang","shanghai");
        Trader li = new Trader("li","beijing");
        Trader zhao = new Trader("zhao","beijing");

        List<Transaction> list = Arrays.asList(
                new Transaction(zhao,2011,300),
                new Transaction(feng,2012,1000),
                new Transaction(feng,2011,400),
                new Transaction(zhang,2012,710),
                new Transaction(zhang,2012,700),
                new Transaction(li,2012,950)
        );
        //找出2011年的所有交易并按照交易额排序
        List<Transaction> res1 = list.stream()
                .filter(tr -> tr.getYear()==2011)
                .sorted(Comparator.comparing(Transaction::getValue))
                .collect(Collectors.toList());
//        System.out.println(res1);


        //找出交易员都在哪些不同的城市工作过
        List<String> res2 = list.stream()
                .map(tr -> tr.getTrader().getCity())
                .distinct()
                .collect(Collectors.toList());
        System.out.println(res2);
        Set<String> res3 = list.stream()
                .map(tr -> tr.getTrader().getCity())
                .collect(Collectors.toSet());
        System.out.println(res3);

        //找出beijing的学员，并按照名字排序
        List<Trader> res4 = list.stream()
                .map(Transaction::getTrader)
                .filter(tr -> tr.getCity().equals("beijing"))
                .distinct()
                .sorted(Comparator.comparing(Trader::getName))
                .collect(Collectors.toList());
        System.out.println(res4);

        //找出所有交易员的姓名字符串，按照字母排序
        String res5 = list.stream()
                .map(tr -> tr.getTrader().getName())
                .distinct()
                .collect(Collectors.joining(","));
        System.out.println(res5);

        //找出是否有在北京工作的
        boolean res6 = list.stream()
                .anyMatch(tr->tr.getTrader().getCity().equals("beijing"));
        System.out.println(res6);

        //打印生活在剑桥的交易员的所有交易额
        list.stream()
                .filter(tr->tr.getTrader().getCity().equals("beijing"))
                .forEach(tr->System.out.println(tr.getValue()));


        //找出最高交易额
        Optional<Integer> res7 = list.stream()
                .map(Transaction::getValue)
                .reduce(Integer::max);
        res7.ifPresent(System.out::println);

        //找出交易额最小的交易
        Optional<Transaction> res8 = list.stream()
                .reduce((a,b)->a.getValue()<b.getValue()?a:b);
        res8.ifPresent(System.out::println);


        /**
         * 数值流 IntStream DoubleStream LongStream
         */
        IntStream intStream = list.stream().mapToInt(Transaction::getValue);
        System.out.println(intStream.sum());
        OptionalInt max = list.stream().mapToInt(Transaction::getValue).max();
        System.out.println(max.orElse(1));
        //生成一个数字0-9的流，range：包含头不包含尾
        IntStream.range(0,10).forEach(System.out::print);
        System.out.println();
        //生成一个数字0-10的流，rangeClosed：包含头且包含尾
        IntStream.rangeClosed(0,10).forEach(System.out::print);
        System.out.println();
//        //把数值流转换为普通流
//        Stream<Integer> stream = intStream.boxed();

        //创建流
        Stream<String> stream = Stream.of("a", "b", "c");
        //创建空流
        Stream<String> emptyStream = Stream.empty();
        //创建无限流，一般需要limit配合去限制流的长度
        Stream.iterate(0, n -> n+2).limit(5).forEach(System.out::print);
        System.out.println();
        Stream.generate(()->1+2).limit(5).forEach(System.out::print);
        Stream.generate(Math::random).limit(5).forEach(System.out::println);
    }
}
