package com.feng.javalearn.stream.api.base;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {


    /**
     * 基础方法：
     * map方法，接收一个函数作为参数，这个函数会被应用到每个元素上，并将其映射为一个新的元素
     * flatMap  把流中的元素都转换为一个流，然后把所有流都连接起来成为一个流
     * anyMatch 返回boolean,判断流中是否有一个元素匹配指定条件
     * allMatch 返回boolean,判断流中所有元素匹配指定条件
     * noneMatch 跟allMatch相反
     * findAny 返回流中的任意元素,
     *      返回类型：Optional，里面有几个方法可以检查值是否存在
     *              isPresent()：有值为true
     *              ifPresent(Consumer<T> block)：会在值存在的时候执行给定的代码，
     *              get()：值存在时返回，否则抛出：NoSuchElement异常
     *              orElse()：值存在时返回，否则返回同一个默认值
     * findFirst 返回第一个元素，跟findAny类似
     */

    public static void main(String[] args) {
        List<Dish> menu = Arrays.asList(
                new Dish("pork", false, 800, Dish.Type.MEAT),
                new Dish("beef", false, 700, Dish.Type.MEAT),
                new Dish("chicken", false, 400, Dish.Type.MEAT),
                new Dish("french fries", true, 530, Dish.Type.OTHER),
                new Dish("rice", true, 350, Dish.Type.OTHER),
                new Dish("season fruit", true, 120, Dish.Type.OTHER),
                new Dish("pizza", true, 550, Dish.Type.OTHER),
                new Dish("prawns", false, 300, Dish.Type.FISH),new Dish("salmon", false, 450, Dish.Type.FISH)
        );

        List<String> threeHighCaloricDishNames = menu.stream()
                .filter(d -> d.getCalories() > 300)//过滤出热量大于300
                .map(Dish::getName)//取出名字
                .limit(3)//取前三个元素
                .collect(Collectors.toList());//转成列表
//        System.out.println(threeHighCaloricDishNames);


//        List<String> names = menu.stream()
//                .filter(d -> {
//                    System.out.println("filtering----"+d.getName());
//                    return d.getCalories() > 300;
//                })//过滤出热量大于300
//                .map(d -> {
//                    System.out.println("mapping----"+d.getName());
//                    return d.getName();
//                })//取出名字
//                .limit(3)//取前三个元素
//                .collect(Collectors.toList());//转成列表
//        System.out.println(names);
        /**
         * 根据names的输出结果可以看出：
         * 尽管很多数据大于300里， 但只输出出了前三个！这是因为limit操作和一种称为短路的技巧
         * filter和map是两个独立的操作，但它们合并到同一次遍历，这叫循环合并
         */

        //menu.stream().forEach(System.out::println);//会打印每个对象


        //筛选出素食
//        List<Dish> vegetarianMenu = menu.stream().filter(Dish::isVegetarian).collect(Collectors.toList());

//        List<Integer> numbers = Arrays.asList(1, 6, 1, 3, 3, 2, 4,10);
//        //过滤出偶数，并去重
//        numbers.stream()
//                .skip(2)//跳过前两个元素不处理
//                .filter(i -> {
//                    System.out.println("filter1--"+i);
//                    return i % 2 == 0;
//                })
//                .limit(3)
//                .distinct()//去除重复元素,根据元素的hashCode和equals方法实现
//                .forEach(System.out::println);


        /**
         * map方法，接收一个函数作为参数，这个函数会被应用到每个元素上，并将其映射为一个新的元素
         */
        List<String> words = Arrays.asList("java 8","feng","in","action");
        List<Integer> lengths = words.stream().map(String::length).collect(Collectors.toList());
        System.out.println(lengths);

        /**
         * flatMap  把流中的元素都转换为一个流，然后把所有流都连接起来成为一个流
         */
//        List<String> collect = words.stream()
//                .map(s -> s.split(""))//将每个单词转换成由其字母构成的数组
//                .flatMap(Arrays::stream) //Arrays.stream()方法接收一个数组，并产生一个流
//                .distinct()
//                .collect(Collectors.toList());
//        System.out.println(collect);

//        List<String[]> list = new ArrayList<>();
//        list.add(new String[]{"aaa","bbb"});
//        list.add(new String[]{"ccc","ddd"});
//        List<String> newList = list.stream().flatMap(Arrays::stream).collect(Collectors.toList());
//        System.out.println(newList);

        List<Integer> num1 = Arrays.asList(1,2,3);
        List<Integer> num2 = Arrays.asList(4,5);
        List<Stream<int[]>> res1 = num1.stream()
                .map(n1 -> num2.stream()
                        .map(n2 -> new int[]{n1, n2}))
                .collect(Collectors.toList());
        List<int[]> res2 = num1.stream()
                .flatMap(n1 -> num2.stream()
                        .map(n2 -> new int[]{n1, n2}))
                .collect(Collectors.toList());

        /**
         * anyMatch 返回boolean,判断流中是否有一个元素匹配指定条件
         * allMatch 返回boolean,判断流中所有元素匹配指定条件
         * noneMatch 跟allMatch相反
         * findAny 返回流中的任意元素,
         *      返回类型：Optional，里面有几个方法可以检查值是否存在
         *              isPresent()：有值为true
         *              ifPresent(Consumer<T> block)：会在值存在的时候执行给定的代码，
         *              get()：值存在时返回，否则抛出：NoSuchElement异常
         *              orElse()：值存在时返回，否则返回同一个默认值
         * findFirst 返回第一个元素，跟findAny类似
         */
//        if(menu.stream().anyMatch(Dish::isVegetarian)){
//            System.out.println("菜单中有素食");
//        }
//        if(menu.stream().allMatch(Dish::isVegetarian)){
//            System.out.println("菜单中有素食");
//        }
//        menu.stream()
//                .filter(Dish::isVegetarian)
//                .findAny()
//                .ifPresent(d -> System.out.println(d.getName()));


        /**
         * reduce：reduce(0,(a,b)->a+b)
         *      接受两个参数
         *          一个是初始值，这里是0
         *          一个BinaryOperator<T>，是个函数式借口，用来将两个元素结合起来产生一个新值
         */
        List<Integer> nums = Arrays.asList(1,2,3);
        int reduce1 = nums.stream()
                .reduce(0, (a, b) -> a + b);
        int reduce2 = nums.stream()
                .reduce(0, Integer::sum);
        System.out.println(reduce1);
        //reduce还有一个重载方法，不接受初始值，返回Optional对象：因为如果没有初始值，考虑到流中没有元素的情况，reduce无法返回数据
        Integer integer = nums.stream()
                .reduce(Integer::sum).orElse(1);
        System.out.println(integer);

    }
}

