package com.feng.javalearn.stream.api.collect;

import com.feng.javalearn.stream.api.base.Dish;

import java.util.*;
import java.util.function.BinaryOperator;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.*;

public class Main {
    public static void main(String[] args) {

        List<Dish> menu = new ArrayList<>();
        menu.addAll(Arrays.asList(
                new Dish("pork", false, 800, Dish.Type.MEAT),
                new Dish("beef", false, 700, Dish.Type.MEAT),
                new Dish("chicken", false, 400, Dish.Type.MEAT),
                new Dish("french fries", true, 530, Dish.Type.OTHER),
                new Dish("rice", true, 350, Dish.Type.OTHER),
                new Dish("season fruit", true, 120, Dish.Type.OTHER),
                new Dish("pizza", true, 550, Dish.Type.OTHER),
                new Dish("prawns", false, 300, Dish.Type.FISH),
                new Dish("salmon", false, 450, Dish.Type.FISH)
        ));


        /**
         * 使用流收集数据
         */
        //counting和count方法，计数
        Long count1 = menu.stream().collect(Collectors.counting());
        long count2 = menu.stream().count();
        System.out.println(count1+"----"+count2);

        //maxBy 求最大值，需要一个比较器对象
        //创建一个比较器对象，comparingInt方法需要ToIntFunction(是个函数式接口，只有一个方法，接受一个T参数返回int)
        Comparator<Dish> dishComparator = Comparator.comparingInt(Dish::getCalories);
        //求出热量最大的菜
        Optional<Dish> res1 = menu.stream().collect(Collectors.maxBy(dishComparator));
        res1.ifPresent(System.out::println);

        /*
            Collectors.summingInt 接受ToIntFunction对象(是个函数式接口，只有一个方法，接受一个T参数返回int)
            averagingInt 求平均数
            summarizingInt 这一个方法涵盖了summingInt，averagingInt，等
         */
        Integer res2 = menu.stream().collect(Collectors.summingInt(Dish::getCalories));
        System.out.println(res2);
        IntSummaryStatistics res3 = menu.stream().collect(Collectors.summarizingInt(Dish::getCalories));
        System.out.println(res3);//输出 IntSummaryStatistics{count=9, sum=4200, min=120, average=466.666667, max=800}

        //joining 连接字符串,可以接受分隔符参数,内部原理是StringBuilder
        String res4 = menu.stream().map(Dish::getName).collect(joining("----"));
        System.out.println(res4);

        /**
         * reducing 需要三个参数
         *      第一个是起始值，也就是流中没有元素的时候返回的值
         *      第二个是Function接口
         *      第三个是BinaryOperator接口,要求参数跟返回结果是同一类型
         */
        BinaryOperator<Integer> bin = Integer::sum;
        Integer res5 = menu.stream().collect(reducing(0,Dish::getCalories,Integer::sum));
        System.out.println(res5);
        Optional<Dish> res6 = menu.stream().collect(reducing((d1, d2) -> d1.getCalories() > d2.getCalories() ? d1 : d2));
        System.out.println(res6);


        /**
         * 分组
         */
        Map<Dish.Type, List<Dish>> res7 = menu.stream().collect(groupingBy(Dish::getType));
        System.out.println(res7);
        //多级分组
        Map<Dish.Type, Map<Dish.Level, List<Dish>>> res8 = menu.stream().collect(groupingBy(Dish::getType, groupingBy(d -> {
            if (d.getCalories() <= 400) return Dish.Level.LOW;
            else if (d.getCalories() <= 700) return Dish.Level.NORMAL;
            else return Dish.Level.HEIGHT;
        })));
        System.out.println(res8);
        //分组计数
        Map<Dish.Type, Long> res9 = menu.stream().collect(groupingBy(Dish::getType, counting()));
        System.out.println(res9);
        //分类求最大值
        Map<Dish.Type, Optional<Dish>> res10 = menu.stream().collect(groupingBy(Dish::getType,
                maxBy(Comparator.comparing(Dish::getCalories))));
        System.out.println(res10);
        //collectingAndThen 把收集器的结果 做后续处理,接受两个参数，一个是需要转换的收集器，一个是转换函数，返回一个收集器
        Map<Dish.Type, Dish> res11 = menu.stream().collect(groupingBy(Dish::getType,
                collectingAndThen(
                        maxBy(Comparator.comparing(Dish::getCalories)),
                        Optional::get
                )));
        System.out.println(res11);

        Map<Dish.Type, Set<Dish.Level>> res12 = menu.stream().collect(groupingBy(Dish::getType,
                mapping(
                        d -> {
                            if (d.getCalories() <= 400) return Dish.Level.LOW;
                            else if (d.getCalories() <= 700) return Dish.Level.NORMAL;
                            else return Dish.Level.HEIGHT;
                        },
                        toSet()
                )));
        System.out.println(res12);

        /**
         * 分区 是分组的特殊情况，由一个返回boolean值得函数作为分组函数
         */
        Map<Boolean, List<Dish>> res13 = menu.stream().collect(partitioningBy(Dish::isVegetarian));
        System.out.println(res13);

        Map<Boolean, Dish> res14 = menu.stream().collect(partitioningBy(Dish::isVegetarian,
                collectingAndThen(
                        maxBy(Comparator.comparing(Dish::getCalories)),
                        Optional::get)));
        System.out.println(res14);

        Integer res15 = menu.stream().collect(collectingAndThen(toList(), List::size));
        System.out.println(res15);

        /**
         * toMap
         */
        Map<String, Integer> res16 = menu.stream().collect(toMap(Dish::getName, Dish::getCalories));
        System.out.println(res16);

        menu.add(new Dish("pork", false, 800, Dish.Type.MEAT));
        Map<String, Integer> res17 = menu.stream().collect(toMap(Dish::getName, Dish::getCalories,
                new BinaryOperator<Integer>() {
                    /**
                     * 该方法处理重复的key，如果有重复的key则把对应的两个value都传进来，自己写处理逻辑
                     *
                     * @param integer
                     * @param integer2
                     * @return
                     */
                    @Override
                    public Integer apply(Integer integer, Integer integer2) {
                        return integer2 + integer;
                    }
                },
                /**
                 * 该方法可以设置返回值类型，该例用的是HashTable,只要是map的子类都行
                 */
                new Supplier<Map<String, Integer>>() {
                    @Override
                    public Map<String, Integer> get() {
//                        return new LinkedHashMap<>();
                        return new Hashtable<>();
                    }
                }));
        //上面的简写方式
        res17 = menu.stream().collect(toMap(Dish::getName, Dish::getCalories,(int1,int2) -> int1+int2 ,Hashtable::new));
        System.out.println(res17);

    }
}
