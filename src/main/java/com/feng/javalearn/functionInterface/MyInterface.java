package com.feng.javalearn.functionInterface;

public interface MyInterface {

    /**
     * 接口中可以定义静态方法
     *   1.必须得有方法体
     *   2.使用方式：接口名.方法名
     *   3.不能被子接口继承
     *   4.不能被实现类复写或者直接调用
     */
    static void staticMethod(){
        System.out.println("接口中定义静态方法，没问题");
    }

    /**
     * 接口中可以定义默认实现方法
     *   1.必须得有方法体
     *   2.使用方式：实现类对象.方法名
     *   3.能被子接口继承
     *   4.能被实现类复写或者直接调用
     */
    default void defaultMethod(){
        System.out.println("接口中的 default 方法");
    }


    static void main(String[] args) {
        MyInterface.staticMethod();
    }

}
