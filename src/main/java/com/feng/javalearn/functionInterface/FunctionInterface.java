package com.feng.javalearn.functionInterface;


/**
 * 函数式接口：一个接口只有一个抽象方法，这个接口被称为函数式接口
 * @FunctionalInterface 注解，用来标识函数式接口，使用这个注解的接口只能有一个抽象法
 * 常见的函数是接口：
 *      java.lang.Runnable
 *      java.util.concurrent.Callable
 *      java.security.PrivilegedAction
 *      java.util.Comparator
 *      java.io.FileFilter
 *      java.nio.file.PathMatcher
 *      java.lang.reflect.InvocationHandler
 *      java.beans.PropertyChangeListener
 *      java.util.EventListener.ActionListener
 *      java.util.EventListener.ChangeListener
 *      java.util.funtion包下的类
 *
 */
@FunctionalInterface
public interface FunctionInterface {
    void test();
}
