package com.feng.javalearn.lambda;

public class Demo {

    public static void main(String[] args) {
        //基本语法：(parameters) -> expression 或者 (parameters) -> {statements}；
        /*
            布尔表达式                (List<String> list) -> list.isEmpty()
            创建对象                  () -> new Apple(10)
            消费一个对象              (Apple a) -> {System.out.println(a.getWeight()); }
            从一个对象中选择/抽取      (String s) -> s.length()
            组合两个值                (int a, int b) -> a * b
            比较两个对象              (Apple a1, Apple a2) -> a1.getWeight().compareTo(a2.getWeight())

            Predicate<T>:函数式接口，仅定义了一个方法：
                public interface Predicate<T>{
                    boolean test (T t);
                }
            函数式接口：就是只定义一个抽象方法的接口

            java.util.function.Consumer<T>定义了一个名叫accept的抽象方法，它接受泛型T 的对象，返回值：void。
            你如果需要访问类型T的对象，并对其执行某些操作，就可以使用 这个接口

            java.util.function.Function<T, R>接口定义了一个叫作apply的方法，它接受一个 泛型T的对象，并返回一个泛型R的对象。
            如果你需要定义一个Lambda，将输入对象的信息映射 到输出，就可以使用这个接口

         */

        final int portNumber = 1337;
        //lambda中使用局部变量，必须设置为：final，但是final可以省略
        Runnable r = () -> System.out.println(portNumber);



        /**
         * 使用 lambda方式实现NumInterface接口
         */
        //常规写法
        NumInterface1 n1 = (int n, int m) -> {
          return n+m;
        };
        /**
         * 简写：
         *   1.参数的数据类型可以不写，会自动推断
         *   2.如果方法中只有一句代码，可以省略“{}”，如果方法有返回值，可以省略"reutrn"
         *   3.如果只有一个参数，可省略“()”
         */
        NumInterface1 n2 = (n, m) -> n+m;

        //更加简写， Integer类中的sum方法跟NumInterface1中的接口都是需要两个参数，且类型相同
        NumInterface1 n3 = Integer::sum;
        //用lambda调用构造方法，Person中的构造方法跟PersonFactory接口的方法参数的个数，类型都一样
        PersonFactory p1 = Person::new;



        new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("线程运行");
            }
        }).start();

        new Thread(() -> {
            System.out.println("线程运行");
        }).start();

        new Thread(() -> System.out.println("线程运行")).start();
    }
}
