package com.feng.javalearn.lambda;

public interface PersonFactory {
    Person createPerson(String name, int age);
}
